Digest job tracker
------------------

Run:

```bash
./gradlew bootRun
```

Build:

```bash
./gradlew clean build
```
